import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { HomeComponent } from '../home/home.component';
import { HttpService } from 'src/app/service/http.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
id:any;
dataDetail;
@ViewChild(HomeComponent, {static: false}) child1:HomeComponent;
  constructor(
    private route: ActivatedRoute,
    private http:HttpService,
  ) { 
    // this.route.queryParams.subscribe((param) => {
    //   if (param.list) {
    //     this.dataDetail = JSON.parse(this.http.createBase64(param.list,"atob"));
    //   }
    // });
  }

  ngOnInit(): void {
    const data = JSON.parse(localStorage.getItem('JsonData'));
    this.id = this.route.snapshot.paramMap.get('id');
    const filterId = data.filter((e)=>e.id == this.id);
    this.dataDetail = filterId[0];
    console.log(this.dataDetail);
  }

}
