import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http: HttpClient,
  ) { }

  async getJson(){
    await this.http.get('https://jsonplaceholder.typicode.com/photos').toPromise().then(data=>{
      localStorage.setItem("JsonData",JSON.stringify(data));
    })
  }
}
