import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router'
import { HttpService} from 'src/app/service/http.service'
import { MatPaginator  } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { LoadingService } from '../service/loading.service';
export interface JsonDataInterface {
  id: number;
  img:any
  title: string;
  data:any
}
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  Jsondata:any;
  displayedColumns: string[] = ['id', 'img','title','data'];
  searchText:any;
  serachlist:any;
  serachiput:any;
  serachiput2:any;
  showspinner = this.loader.loading$;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private router:Router,
    private http:HttpService,
    private loader:LoadingService
    ) {}

  ngOnInit(){
    this.http.getJson()
    this.Jsondata = new MatTableDataSource<JsonDataInterface>(JSON.parse(localStorage.getItem('JsonData')))
    this.serachlist = this.Jsondata
    console.log(this.serachlist)
  }

  ngAfterViewInit() {
    this.serachlist.paginator = this.paginator;
  }

  async SearchBar(event) {
    this.serachiput = (event.target as HTMLInputElement).value;
    this.serachlist.filter =  this.serachiput.trim().toLowerCase();

    if (this.serachlist.paginator) {
      this.serachlist.paginator.firstPage();
    }
  }

  async Search(event){
    let item = [];
    this.serachiput2 = (event.target as HTMLInputElement).value;
    console.log(this.serachiput2)
    console.log(this.Jsondata.filteredData)
    item = !!this.serachiput2 ? await this.Jsondata.filteredData.filter((e)=> 
    e.title.indexOf(this.serachiput2.trim()) !== -1 ||
    ((e.id.toString())).indexOf(this.serachiput2.trim()) !== -1 
    ):this.Jsondata;
    console.log(item)
    
    this.serachlist = item 
    if (this.serachlist.paginator) {
      this.serachlist.paginator.firstPage();
    }
  }

  async selectdata(data){
    await this.router.navigate(['/Detail',data.id])
    }
}
