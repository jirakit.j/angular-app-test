import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../app/home/home.component'
import { DetailComponent } from '../app/detail/detail.component'
const routes: Routes = [
  { path: '', redirectTo: '/Home', pathMatch: 'full'},
  { path:'Home', component:HomeComponent},
  { path:'Detail/:id', component:DetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [HomeComponent,DetailComponent]